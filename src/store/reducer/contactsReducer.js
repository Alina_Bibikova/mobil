import {FETCH_CONTACTS_FAILURE, FETCH_CONTACTS_REQUEST, FETCH_CONTACTS_SUCCESS, ADD_CONTACT} from "../actions/actionTypes";

const initialState = {
    contacts: {},
    loading: true,
    error: null
};

const contactsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_CONTACTS_REQUEST:
            return {
                ...state,
                loading: true
            };

        case FETCH_CONTACTS_SUCCESS:
            return {
                ...state,
                contacts: action.contacts,
                loading: false
            };

        case FETCH_CONTACTS_FAILURE:
            return {
                ...state,
                error: action.error,
                loading: false
            };


        case ADD_CONTACT:
            return {
                ...state,
                contacts: {
                    ...state.contacts,
                    [action.id]: state.contacts[action.id]
                },
            };

        default:
            return state
    }
};

export default contactsReducer;
