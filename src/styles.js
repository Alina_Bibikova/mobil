import {StyleSheet} from "react-native";

export default styles = StyleSheet.create({
container: {
    paddingTop: 30,
    flex: 1,
    backgroundColor: '#000'
},
contact: {
    marginBottom: 10,
    backgroundColor: "#781E27",
    flexDirection: 'row',
    alignItems: 'center'
},
contactName: {
    color: '#fff',
    fontSize: 24,
    marginBottom: 5
},
contactImg: {
    width: 100,
    height: 100,
    marginRight: 10
},

modal: {
    flex: 1,
    paddingTop: 30,
    paddingBottom: 10,
    paddingHorizontal: 10,
    justifyContent: 'space-between',
},
modalTitle: {
    fontSize: 30,
    fontWeight: 'bold',
    marginBottom: 20
},
modalContact : {
    marginBottom: 5,
    flexDirection: 'row',
    justifyContent: 'space-between'
},
modalText: {
    fontSize: 20,
},
loader: {
    marginTop: 100,
},
form: {
    marginTop: 30
},
formTitle: {
    fontSize: 18,
    marginBottom: 5
},
});
