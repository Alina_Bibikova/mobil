import React from 'react';
import {Modal, View, Text} from "react-native";
import styles from '../../styles';

const Contact = props => {
    return (
        <Modal
            animationType="slide"
            transparent={false}
            visible={props.show}
            onRequestClose={props.onToggleModal}
        >
            <View style={styles.modal}>
                <View>
                    {Object.keys(props.contacts).map(contactId => {
                        let contacts = null;

                        if (props.contacts[contactId] > 0) {
                            contacts = (
                                <View key={contactId} style={styles.modalContact}>
                                    <Text style={styles.modalText}>{props.contacts[contactId].photo}</Text>
                                    <Text style={styles.modalText}>{props.contacts[contactId].name}</Text>
                                    <Text style={styles.modalText}>{props.contacts[contactId].email}</Text>
                                    <Text style={styles.modalText}>{props.contacts[contactId].phone}</Text>
                                </View>
                            )
                        }
                        return contacts
                    })}

                </View>
                <View>
                    <Button
                        onPress={props.onToggleModal}
                        title="Back to list"
                    />
                </View>
            </View>
        </Modal>
    );
};

export default Contact;