import React, {Component} from 'react';
import {connect} from "react-redux";
import {addContacts, fetchContacts} from "../store/actions/actionsContacts";
import {View, FlatList, Text, Image, TouchableOpacity} from 'react-native'
import Contact from "../components/Contact/Contact";
import styles from '../styles';

class Contacts extends Component {
    state = {
        showModal: false,
    };

    componentDidMount() {
        this.props.fetchContacts();
    }

    toggleModal = () => {
        this.setState({showModal: !this.state.showModal})
    };

    // pullToRefresh = () => {
    //     this.props.fetchContacts();
    // };

    convertToArr = obj => {
        return Object.keys(obj).map(id => {
            return {...obj[id], id};
        });
    };

    render() {

        if (this.props.error) {
            return <Text>{this.props.error}</Text>
        }

        const contacts = (
            <View style={styles.container}>
                <FlatList
                    // onRefresh={() => this.pullToRefresh()}
                    data={this.convertToArr(this.props.contacts)}
                    keyExtractor={item => item.id}
                    renderItem={({item}) => (
                        <TouchableOpacity style={styles.contact}>
                            onPress={() => this.props.addContacts(item.id)}
                            <Image
                                style={styles.contactImg}
                                source={{uri: item.image}}
                            />
                            <Text style={styles.contactName}>{item.name}</Text>
                        </TouchableOpacity>
                    )}
                />
                <Contact
                    show={this.state.showModal}
                    photo={this.props.photo}
                    name={this.props.name}
                    onToggleModal={this.toggleModal}
                    email={this.props.email}
                    phone={this.props.phone}
                />
            </View>
        );

        return this.props.loading ? <ActivityIndicator style={styles.loader} size="large" />
            : contacts;
    }

}

const mapStateToProps = state => ({
    contacts: state.contacts,
    loading: state.loading
});

const mapDispatchToProps = dispatch => ({
    fetchContacts: () => dispatch(fetchContacts()),
    addContacts: (id) => dispatch(addContacts(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Contacts);