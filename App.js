import React from 'react';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from "redux";
import thunkMiddleware from 'redux-thunk';
import contactsReducer from "./src/store/reducer/contactsReducer";

import Contacts from "./src/containers/Contacts";

const store = createStore(contactsReducer, applyMiddleware(thunkMiddleware));

export default class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <Contacts />
            </Provider>
        );
    }
}
